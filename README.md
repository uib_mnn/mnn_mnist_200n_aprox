# MNN MNIST Aprox
Este proyecto se ha creado para simular la red MNN de 200 neuronas para MNIST dataset utilizando sumadores __Aproximados__.

Solo se han aproximado los sumadores de la primera capa de la red. Los sumadores utilizados para la capa lineal (para el `APC`), no se han aproximado.

Dentro del repo estan todos los ficheros fuentes en `.vhd` salvo el testbench que está escrito en `verilog` : `tb_maxmin_top_verilog.v`. 

En la subcarpeta `vcd` he puesto la última simulación que realicé usando el `ModelSim` para comprobar con el `vcd` generado por la herramienta de sintesis de `CADENCE`. 

Además de esto, se ha agregado el comando para la herramienta `XCELIUM` para simular el RTL en `CADENCE` (ver [XCELIUM_command.txt](src/XCELIUM_command.txt)).

# Métricas
- Accuracy en Software : `97.4%`
- Accuracy en Hardware (using SC): `96.81%`, utilizando sumadores aproximados de tipo `OR` para los 5 bits LSB. 

# Diagrama de diseño top 

### Entity: maxmin_top
![Diagram](_Doc/maxmin_top.svg "Diagram")

