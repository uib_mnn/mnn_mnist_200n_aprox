LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;


ENTITY edge_detector_up IS     
    PORT(
        -- inputs
        clk     : in std_logic;
        input   : in std_logic;
        output  : out std_logic
   );
END edge_detector_up;

ARCHITECTURE Behavioral OF edge_detector_up IS
    
    signal q1, q2 : std_logic := '0';
    
BEGIN
    
    process (clk)
    begin
        if (rising_edge(clk)) then
            q1 <= input;
            q2 <= q1;
        end if;
    end process;
     
    output <= q1 and not q2;
    
END Behavioral;



    
