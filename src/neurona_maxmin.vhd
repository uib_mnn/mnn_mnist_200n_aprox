LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE work.maxmin_net_pkg.all;
USE work.MyTypes.all;

---------------------------------------------------------------
-- Version		Authors				Date				Changes
---------------------------------------------------------------
-- 0.1 			CCFF             27/10/2021          	Initial

--  constant MAXMIN_NEURON_TYPE_MAX : integer := 0;
-- 	constant MAXMIN_NEURON_TYPE_MIN : integer := 1;

--=============================================================

ENTITY neurona_maxmin IS
    generic (				
        GNRC_N_INPUTS                   : integer   :=4; 
        GNRC_MAXMIN_TYPE                : INTEGER   := 0;        
        GNRC_ADDITION_SCALE_DOWN        : INTEGER   := 2
    );
    PORT
    (
        -- Inputs
        clock : IN STD_LOGIC; 
        reset : IN STD_LOGIC;		
        
        i_lfsr : 		IN signed (PKG_BIT_PRECISSION-1 DOWNTO 0);
        i_x : 			IN pkg_xi_neuron_maxmin_array_t(0 to GNRC_N_INPUTS-1);
        i_w : 			IN pkg_xi_neuron_maxmin_array_t(0 to GNRC_N_INPUTS-1);
    
        --Outputs
        o_y_sc : 		OUT STD_LOGIC		
    );
    
END neurona_maxmin;

ARCHITECTURE arch OF neurona_maxmin IS
    constant ADDER_TYPE         : integer := PKG_ADDER_TYPE_SELECTED;
    constant ADDER_APP_N_BITS   : integer := PKG_ADDER_APP_N_BITS_SELECTED;

    signal sum_array        	: pkg_sum_array_t           (0 to GNRC_N_INPUTS-1);
    signal sum_array_scaled     : pkg_sum_scaled_array_t    (GNRC_N_INPUTS-1 downto 0);
    signal sum_sc               : std_logic_vector          (GNRC_N_INPUTS-1 downto 0);

    component loa_adder is
        generic(
            in_wid : integer := 8;
            lowbits_size : integer := 4
        );
        port(
            i_a : in    signed (in_wid-1 downto 0);
            i_b : in    signed (in_wid-1 downto 0);
            i_c : out   signed (in_wid downto 0)
        );
    end component;

BEGIN

    Gen_2 : for I in 0 to GNRC_N_INPUTS-1 generate
        -- STD adder
        gen_adder_std: if ADDER_TYPE = PKG_ADDER_TYPE_STD generate
            sum_array(I) <= resize(i_x(I), PKG_INPUT_WIDTH_BITS+1) + resize(i_w(I), PKG_INPUT_WIDTH_BITS);
        end generate;
        -- Or App adder
       gen_adder_app_or: if ADDER_TYPE = PKG_ADDER_TYPE_APP_OR generate
           adder_app_inst : loa_adder
           generic map (in_wid => PKG_INPUT_WIDTH_BITS, lowbits_size => ADDER_APP_N_BITS)    
        --    port map(tmp, tmp1, tmp2);
           port map(i_x(I), i_w(I), sum_array(I));
       end generate;
    end generate;
    ------------------------------------------------
    -- Addition Layer
    ------------------------------------------------
    process (sum_array)
    begin        
        for i in 0 to GNRC_N_INPUTS-1 loop
            
            sum_array_scaled(i) <= sum_array(i)(PKG_INPUT_WIDTH_BITS DOWNTO 1);
            
        end loop;        
    end process;

    -- process (sum_array)
    -- begin        
    --     for i in 0 to GNRC_N_INPUTS-1 loop
            
    --         sum_array_scaled(i) <= sum_array(i)(PKG_INPUT_WIDTH_BITS-1 DOWNTO 0);
    --         -- CLAMP
    --         if sum_array(i) > (2**(PKG_BIT_PRECISSION-1)-1) then
    --             sum_array_scaled(i) <= to_signed(2**(PKG_BIT_PRECISSION-1)-1, PKG_INPUT_WIDTH_BITS);
    --         end if;
    --         if sum_array(i) < -(2**(PKG_BIT_PRECISSION-1)-1) then
    --             sum_array_scaled(i) <= to_signed(-1*(2**(PKG_BIT_PRECISSION-1)-1), PKG_INPUT_WIDTH_BITS);
    --         end if;
    --     end loop;        
    -- end process;

    ------------------------------------------------
    -- SUM conversion to SC
    ------------------------------------------------
    b2p_sum_inst: FOR index IN sum_sc'RANGE GENERATE 
        sum_sc(index) <= '1' when sum_array_scaled(index) >= i_lfsr else '0';
    END GENERATE;

    ------------------------------------------------
    -- MAX or MIN Output SC 
    ------------------------------------------------
    gen_max_output: if GNRC_MAXMIN_TYPE = PKG_MAXMIN_NEURON_TYPE_MAX generate
        process (sum_sc)
            variable TMP : std_logic;
        begin
            TMP := '0';
            for I in sum_sc'RANGE loop
                TMP := TMP or sum_sc(I);
            end loop;
            o_y_sc <= TMP;
        end process;		
    end generate gen_max_output;

    gen_min_output: if GNRC_MAXMIN_TYPE = PKG_MAXMIN_NEURON_TYPE_MIN generate
        process (sum_sc)
            variable TMP : std_logic;
        begin
            TMP := '1';
            for I in sum_sc'RANGE loop
                TMP := TMP and sum_sc(I);
            end loop;
            o_y_sc <= TMP;
        end process;		
    end generate gen_min_output;
    

END arch;
