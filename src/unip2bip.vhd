-- autogenerate, unip2bip v1
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
--use work.MyTypes.all;

ENTITY unip2bip IS 
    GENERIC(
      INPUT_WIDTH : INTEGER := 4;
		APC_MAX : INTEGER := 1000
    );
    PORT(
        -- inputs
        unip 		: in unsigned(INPUT_WIDTH-1 downto 0); 
        -- outputs
        bip			: out signed(INPUT_WIDTH downto 0)
   );
END unip2bip;

ARCHITECTURE Behavioral OF unip2bip IS
    
	signal num_ones, num_zeros : signed(INPUT_WIDTH downto 0);
	
BEGIN

   num_ones <= '0' & signed(unip(unip'range)); 

   num_zeros <= to_signed(APC_MAX, num_zeros'length) - num_ones;

   bip <= num_ones - num_zeros;
  
    
END Behavioral;



    
