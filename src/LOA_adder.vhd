----------------------------------------------------------------------------------
-- Company: University of Alberta
-- Engineer: Bowen Liu
-- 
-- Create Date: 10/13/2022 10:24:35 AM
-- Design Name: lower-part OR adder
-- Module Name:  LOA_adder - Behavioral
-- Project Name: Approximate Adders
-- Target Devices: 
-- Tool Versions: 
-- Description: This is the LOA adder design 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity loa_adder is
    generic(
        in_wid : integer := 8;
        lowbits_size : integer := 4
    );
    port(
        i_a : in    signed (in_wid-1 downto 0);
        i_b : in    signed (in_wid-1 downto 0);
        i_c : out   signed (in_wid downto 0)
    );
end loa_adder;

architecture behavioral of loa_adder is
    -- alias i_a_top : signed (in_wid-lowbits_size-1 downto 0) is i_a(in_wid-1 downto lowbits_size);
    -- alias i_a_bot : signed (lowbits_size-1 downto 0) is i_a(lowbits_size-1 downto 0);
    -- alias i_b_top : signed (in_wid-lowbits_size-1 downto 0) is i_b(in_wid-1 downto lowbits_size);
    -- alias i_b_bot : signed (lowbits_size-1 downto 0) is i_b(lowbits_size-1 downto 0);
    -- alias i_c_top : signed (in_wid-lowbits_size downto 0) is i_c(in_wid downto lowbits_size);
    -- alias i_c_bot : signed (lowbits_size-1 downto 0) is i_c(lowbits_size-1 downto 0);

    signal i_a_top : signed (in_wid-lowbits_size-1 downto 0);
    signal i_a_bot : signed (lowbits_size-1 downto 0);
    signal i_b_top : signed (in_wid-lowbits_size-1 downto 0);
    signal i_b_bot : signed (lowbits_size-1 downto 0);
    signal i_c_top : signed (in_wid-lowbits_size downto 0);
    signal i_c_bot : signed (lowbits_size-1 downto 0);

    signal carry : signed (in_wid-lowbits_size downto 0);
    
begin

    i_a_top <= i_a(in_wid-1 downto lowbits_size);
    i_a_bot <= i_a(lowbits_size-1 downto 0);

    i_b_top <= i_b(in_wid-1 downto lowbits_size);
    i_b_bot <= i_b(lowbits_size-1 downto 0);

    i_c(in_wid downto lowbits_size) <= i_c_top;
    i_c(lowbits_size-1 downto 0) <= i_c_bot;

    lower_bits_or : process (i_a_bot, i_b_bot) is    
    begin 
        for i in lowbits_size-1 downto 0 loop
            i_c_bot(i) <= i_a_bot(i) or i_b_bot(i);
        end loop;        
    end process;

    carry(0) <= i_a_bot(lowbits_size-1) and i_b_bot(lowbits_size-1);
    carry(in_wid-lowbits_size downto 1) <= (others => '0');

    i_c_top <= resize(i_a_top,i_c_top'length) + resize(i_b_top,i_c_top'length) + carry;
    
end behavioral;


