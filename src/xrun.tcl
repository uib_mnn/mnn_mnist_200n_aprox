set pack_assert_off { std_logic_arith numeric_std }
# dumptcf -output tb/dump.tcf -dumpwireasnet -overwrite
# call vcdfile testpatterns.vcd -t ns
# database -open -vcd vcddb -default -timescale ns
# probe -create -all -depth all
# probe -create top_module_name -depth all
# probe -create maxmin_top.vhd edge_detector.vhd
run 
dumptcf -end

exit